## 4 Saisons ##

Le but du jeu est de disposer 12 sachets de graines dans douze emplacements repérés selon les saisons, la difficulté étant que chaque sachet correspond à un emplacement précis. Les sachets en place déclenchent une trappe. L'idée qui est venue au cours de la réunion est d'utiliser le placement précis d'aimants sur une grille, et de positionner des capteurs à effet Hall alignés avec ces aimants (schéma à venir pour plus de clarté). Le concept a besoin d'être testé…

Si l'on décompose l'algorithme: → Les douze sachets sont disposés au bon endroit → une électroaimant ou un moteur libère une trappe. Matériel électronique nécessaire:

1 arduino
12 capteurs à effet Hall (https://www.gotronic.fr/art-capteur-a-effet-hall-st054-26119.htm)
12 aimants (https://www.gotronic.fr/art-aimant-o8-x-3-mm-17973.htm)
1 électro aimant
1 relais
1 alim 5V et 1 alim 12V

Construction à prévoir:

boîte en bois avec 12 compartiments
trappe et système d'ouverture

