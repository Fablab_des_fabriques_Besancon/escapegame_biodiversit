# EscapeGame_Biodiversité

3 jeux qui sont autant de défis à réaliser pendant l'escape game organisé par la direction en Éducation à l'environnement et au 
Développement Durable de la Ville de Besançon, pendant les JPO de l'Orangerie à partir du 28 septembre 2020. 

L'ensemble du projet est documenté: https://wiki-fablab.grandbesancon.fr/doku.php?id=escape_game