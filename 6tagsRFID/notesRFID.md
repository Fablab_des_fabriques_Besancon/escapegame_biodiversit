Escape Game RFID*6

6 objets collectés au cours de l'escape game doivent être associés par paire pour permettre aux joueurs de finir l'aventure. Il faut détecter le positionnement des éléments, mais là, la difficulté va être de détecter les paires, où qu'elles soient positionnées. La réussite allume une lumière en haut de la porte de sortie. La solution technique la plus adaptée semble l'utilisation de tags RFID. Il semblerait que 6 * MFRC522 soit utilisables avec un arduino UNO. Ce que l'on cherche à faire est très proche de ceci.

Les six objets en question devront être: résistants à la terre (car ils seront plongés dans un faux compost), faciles à faire rouler dans un chéneau, et porteurs d'une illustration. Ils sont à fabriquer au fablab.

Matériel électronique nécessaire:

un arduino Uno
6 capteurs RFID GoTronic et leurs tags
un relais pour allumer la lampe
une alim 5V + une alim 3.3V et une alim pour la lampe

Premier test

Usage de la lib MFRC522

wiring avec un wemos:

MFRC522 	Wemos
RST   	D0
SDA(SS) 	D8
MOSI 	D7
MISO 	D6
SCK 	D5

Les deux lignes de déclaration de pin vont se transformer et devenir:

#define SS_PIN D8
#define RST_PIN D0

Une fois uploadé, les essais de présentation des tags devant l'antenne nous donnent immédiatement l'identifiant unique de chaque tag, en hexa et en décimal !

PICC type: MIFARE 1KB
A new card has been detected.
The NUID tag is:
In hex:  76 71 D5 E5
In dec:  118 113 213 229

PICC type: MIFARE 1KB
A new card has been detected.
The NUID tag is:
In hex:  27 17 B2 C5
In dec:  39 23 178 197

