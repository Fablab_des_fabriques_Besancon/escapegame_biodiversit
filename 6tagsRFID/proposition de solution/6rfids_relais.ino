#include <SPI.h>
#include <MFRC522.h>

// PIN des lecteurs RFID
#define RST_PIN       9
#define A1_PIN       10
#define A2_PIN        8
#define B1_PIN        7
#define B2_PIN        6
#define C1_PIN        5
#define C2_PIN        4

#define RELAIS_PIN    3

#define NB_LECTEURS   6

int valeur;

int table_valeurs[NB_LECTEURS];

byte lectPINS[] = {A1_PIN, A2_PIN, B1_PIN, B2_PIN, C1_PIN, C2_PIN};

// On crée les instances MFRC522 :
MFRC522 mfrc522[NB_LECTEURS];

/* Initialisation */

void setup() {

  Serial.begin(9600);           // Liaison série avec le PC

  SPI.begin();                  // Lance la connection SPI



  /* on cherche les lecteurs */
  for (uint8_t lecteur = 0; lecteur < NB_LECTEURS; lecteur++) {
    mfrc522[lecteur].PCD_Init(lectPINS[lecteur], RST_PIN);
    Serial.print(F("Lecteur n° "));
    Serial.print(lecteur);
    Serial.print(F(": "));
    mfrc522[lecteur].PCD_DumpVersionToSerial();
    //mfrc522[lecteur].PCD_SetAntennaGain(mfrc522[reader].RxGain_max);

    digitalWrite(RST_PIN, LOW);    // "mise en veille" des lecteurs.
  }

  pinMode(RELAIS_PIN, OUTPUT);
  digitalWrite(RELAIS_PIN, LOW);
}

/* fonction qui lit les UIDs et associe la valeur numérique définie */
void dump_byte_array(byte * buffer, byte bufferSize) {
  String content = "";
  for (byte i = 0; i < bufferSize; i++) {
    Serial.print(buffer[i] < 0x10 ? " 0" : " ");
    Serial.print(buffer[i], HEX);
    content.concat(String(buffer[i] < 0x10 ? " 0" : " "));
    content.concat(String(buffer[i], HEX));
    content.toUpperCase();
    if (content.substring(1) == "C9 04 00 98")  {
      valeur = 1;
    }
    if (content.substring(1) == "4A 93 67 1A")  {
      valeur = 2;
    }
    if (content.substring(1) == "B9 BE 05 98")  {
      valeur = 3;
    }
    if (content.substring(1) == "4A EF 70 19")  {
      valeur = 7;
    }
    if (content.substring(1) == "89 0C F7 98")  {
      valeur = 8;
    }
    if (content.substring(1) == "D9 57 32 88")  {
      valeur = 9;
    }
  }
}

/* fonction qui réinitialise les valeurs de la table à 0 */
void reset_valeurs() {
  for (uint8_t lecteur = 0; lecteur < NB_LECTEURS; lecteur++) {
    table_valeurs[lecteur] = 0;
  }
}

/* Boucle générale */

void loop() {

  for (uint8_t lecteur = 0; lecteur < NB_LECTEURS; lecteur++) {
    digitalWrite(RST_PIN, HIGH);   // "réveil" du lecteur.
    mfrc522[lecteur].PCD_Init();   // initialisation du lecteur
    // Looking for new cards
    if (mfrc522[lecteur].PICC_IsNewCardPresent() && mfrc522[lecteur].PICC_ReadCardSerial()) {

      Serial.print(F("Lecteur n° "));
      Serial.print(lecteur);

      // Affiche les UIDs des lecteurs
      Serial.print(F(": UID:"));
      dump_byte_array(mfrc522[lecteur].uid.uidByte, mfrc522[lecteur].uid.size);
      //        mfrc522[lecteur].PICC_HaltA();
      // mfrc522[lecteur].PCD_StopCrypto1();
      table_valeurs[lecteur] = valeur;
      Serial.println();
      Serial.print("Valeur : ");

      Serial.println(table_valeurs[lecteur]);
      Serial.println();

    }

    digitalWrite(RST_PIN, LOW);    // "mise en veille" du lecteur.
    delay (200);
    if ((table_valeurs[0] + table_valeurs[1] == 10) && (table_valeurs[2] + table_valeurs[3] == 10) && (table_valeurs[4] + table_valeurs[5] == 10)) {
      Serial.println("Victoire");
      Serial.println();
      digitalWrite(RELAIS_PIN, HIGH);
      delay (5000);
      reset_valeurs();
    }
    else {
      digitalWrite(RELAIS_PIN, LOW);
    }
  }
}
